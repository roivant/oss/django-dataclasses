import json

from test_project import models


def test_my_view(admin_client):
    response = admin_client.post(
        "/test_post/", json.dumps({"a": "string", "b": 3}), content_type="application/json"
    )
    assert response.status_code == 201, response.content
    assert response.json() == {"c": {"e": ["2000-01-01T00:00:00Z"]}, "d": 5}


def test_other_view(admin_client):
    response = admin_client.get("/test_get/2")
    assert response.status_code == 200, response.content
    assert response.json() == {"c": {"e": []}, "d": 2}


def test_iterable_view(admin_client):
    response = admin_client.get("/test_iterable/")
    assert response.status_code == 200
    assert response.json() == {"count": 1, "page": 1, "items": [{"a": 100}]}


def test_paginator_view(admin_client):
    models.MyModel.objects.bulk_create([models.MyModel(a=x) for x in range(20)])
    response = admin_client.get("/test_paginator/?page=1")
    assert response.status_code == 200, response.content
    assert response.json() == {
        "count": 20,
        "page": 1,
        "items": [
            {"a": 0},
            {"a": 1},
            {"a": 2},
            {"a": 3},
            {"a": 4},
            {"a": 5},
            {"a": 6},
            {"a": 7},
            {"a": 8},
            {"a": 9},
        ],
    }

    response = admin_client.get("/test_paginator/?page=2")
    assert response.status_code == 200
    assert response.json() == {
        "count": 20,
        "page": 2,
        "items": [
            {"a": 10},
            {"a": 11},
            {"a": 12},
            {"a": 13},
            {"a": 14},
            {"a": 15},
            {"a": 16},
            {"a": 17},
            {"a": 18},
            {"a": 19},
        ],
    }
