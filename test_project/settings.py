# these settings are only used for running tests

SECRET_KEY = "JUST A TEST FOLKS"
DEBUG = True

DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": "sqlite.db"}}

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "test_project",
    "django_dataclasses",
]

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
]

# ALLOWED_HOSTS = "*"

ROOT_URLCONF = "test_project.urls"
