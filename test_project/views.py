import datetime
import typing

from django import http

import django_dataclasses
from test_project import models


@django_dataclasses.login_required
def auth(request: http.HttpRequest):
    return http.HttpResponse("hello")


@django_dataclasses.dataclass
class MyViewRequest:
    a: str
    b: int


@django_dataclasses.dataclass(frozen=True)
class NestedThing:
    e: typing.List[datetime.datetime]


@django_dataclasses.dataclass
class MyViewResponse:
    """MyViewResponse OpenAPI description"""

    c: NestedThing
    d: int


@django_dataclasses.dataclass
class IterableObject:
    """An object that is returned as part of a list"""

    a: int


@django_dataclasses.dataclass
class SortedPaginatedResponse:
    """SortedPaginatedResponse OpenAPI description"""

    page_size: int
    page_num: int
    sort_type: str
    sort_column: str
    total_rows: int
    data: typing.List[int]


@django_dataclasses.dataclass
class QueryParams:
    """All query params must be strings as they are encoded in the URL"""

    page: str = "1"
    page_size: str = "10"


@django_dataclasses.login_required
@django_dataclasses.post
def my_view(_: http.HttpRequest, body: MyViewRequest) -> MyViewResponse:
    """my_view OpenAPI description"""
    return MyViewResponse(NestedThing([datetime.datetime(2000, 1, 1)]), body.b + 2)


@django_dataclasses.get
def other_view(_: http.HttpRequest, item: int) -> MyViewResponse:
    """no body"""
    return MyViewResponse(NestedThing([]), item)


SomeIterable = django_dataclasses.iterable_factory(IterableObject)


@django_dataclasses.get
def iterable_view(_: http.HttpRequest) -> SomeIterable:
    """Wraps result into a list of items"""
    return SomeIterable(items=[IterableObject(a=100)])


SomePaginator = django_dataclasses.iterable_factory(IterableObject, paginate=True)


@django_dataclasses.get
def paginator_view(_, query: QueryParams) -> SomePaginator:
    """Wraps result into a paginated list of items"""
    queryset = models.MyModel.objects.all()
    page = SomePaginator(items=queryset, page_num=int(query.page), page_size=int(query.page_size))
    page.items = [IterableObject(row.a) for row in page.items]
    return page
