from django.urls import path

from test_project import views

urlpatterns = [
    path("auth/", views.auth),
    path("test_post/", views.my_view),
    path("test_get/<int:item>", views.other_view),
    path("test_iterable/", views.iterable_view),
    path("test_paginator/", views.paginator_view),
]
